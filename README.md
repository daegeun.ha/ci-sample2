# 도커 환경을 위한 Django 샘플 프로젝트
[![pipeline status](https://gitlab.com/daegeun.ha/ci-sample2/badges/master/pipeline.svg)](https://gitlab.com/daegeun.ha/ci-sample2/commits/master)
[![coverage report](https://gitlab.com/daegeun.ha/ci-sample2/badges/master/coverage.svg)](https://gitlab.com/daegeun.ha/ci-sample2/commits/master)

### 요약

```
$ git clone https://gitlab.com/raccoony/ci-sample.git
$ cd ci-sample
```

### 요구조건

- 도커 엔진 : 1.12.0 이상
- 도커 컴포즈 : 1.6.0 이상

### 실행

1. 저장소 클론

```
$ git clone https://gitlab.com/raccoony/ci-sample.git
```

2. 소스 디렉터리로 이동

```
$ cd ci-sample
```
